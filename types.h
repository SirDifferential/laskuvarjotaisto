#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>
#include "standard_includes.h"

namespace lvt
{

typedef uint8_t _UInt8;
typedef int8_t _Int8;

typedef uint16_t _UInt16;
typedef int16_t _Int16;

typedef uint32_t _UInt;
typedef int32_t _Int;
typedef uint32_t _UInt32;
typedef int32_t _Int32;

typedef uint64_t _UInt64;
typedef int64_t _Int64;

typedef std::shared_ptr<sf::RenderWindow> RenderWindowPtr;
typedef std::shared_ptr<sf::Texture> TexturePtr;
typedef std::shared_ptr<sf::Sprite> SpritePtr;
typedef std::shared_ptr<sf::Music> MusicPtr;
typedef std::shared_ptr<sf::Sound> SoundPtr;
typedef std::shared_ptr<sf::SoundBuffer> SoundBufferPtr;
typedef std::shared_ptr<sf::Text> TextPtr;
typedef std::shared_ptr<sf::Font> FontPtr;

class Scene;
class Dude;
class ChuteSprite;

typedef std::shared_ptr<Scene> ScenePtr;
typedef std::shared_ptr<Dude> DudePtr;
typedef std::shared_ptr<ChuteSprite> ChuteSpritePtr;

enum EVENT
{
    EVENT_UP = 1,
    EVENT_DOWN = 2,
    EVENT_ENTER = 3,
    EVENT_ESCAPE = 4,
    EVENT_SHIFT = 5,
    EVENT_Z = 6,
    EVENT_X = 7,
    EVENT_C = 8,

    EVENT_LEFT_IS_PRESSED = 100,
    EVENT_RIGHT_IS_PRESSED = 101,
    EVENT_UP_IS_PRESSED = 102,
    EVENT_DOWN_IS_PRESSED = 103,

    EVENT_JOYSTICK_X_AXIS = 200,
    EVENT_JOYSTICK_Y_AXIS = 201,
    EVENT_JOYSTICK_Z_AXIS = 202,
    EVENT_JOYSTICK_R_AXIS = 203,

    EVENT_UNKNOWN = 9999
};

enum CONTROL_CODE
{
    CONTROL_PUNCH = 0,
    CONTROL_GRAB = 1,
    CONTROL_START = 2,
    CONTROL_SELECT = 3,
    CONTROL_UNUSED = 9999
};

} // namespace

#endif // TYPES_H
