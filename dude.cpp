#include "dude.h"
#include "utils.h"
#include "chutesprite.h"
#include "datastorage.h"

namespace lvt {

Dude::Dude(int p_id)
{
    id = p_id;
    position.x = 50 + ((float)p_id * 10);
    position.y = 100;
    velocity.x = 0;
    velocity.y = 0;
    acceleration.x = 0;
    acceleration.y = 0;
    acceleration_slowdown.x = 0.05f;
    acceleration_slowdown.y = 0.05f;
    max_velocity.x = MAX_VELOCITY_X;
    max_velocity.y = TERMINAL_VELOCITY_HOLDING;
    has_chute = false;
    moved_left = false;
    moved_right = false;
    moved_up = false;
    moved_down = false;
    punched = false;
    punchMeter = 0.f;
    isDizzy = false;
    chute_open = false;

    controls_disabled = false;

    lastFacing = Right;
}

int Dude::loadSprite(ChuteSpritePtr& ptr, const std::string& name) {
    ptr = dataStorage.getSprite(name);
    if (ptr == nullptr) {
        fprintf(stderr, "failed getting sprite: %s\n", name.c_str());
        return 1;
    }
    return 0;
}

int Dude::init()
{
    if (loadSprite(gfx_top, "dude_normal_up") != 0) return 1;
    if (loadSprite(gfx_topleft, "dude_normal_upleft") != 0) return 1;
    if (loadSprite(gfx_topright, "dude_normal_upright") != 0) return 1;
    if (loadSprite(gfx_left, "dude_normal_left") != 0) return 1;
    if (loadSprite(gfx_right, "dude_normal_right") != 0) return 1;
    if (loadSprite(gfx_down, "dude_normal_down") != 0) return 1;
    if (loadSprite(gfx_downleft, "dude_normal_downleft") != 0) return 1;
    if (loadSprite(gfx_downright, "dude_normal_downright") != 0) return 1;

    if (loadSprite(gfx_punch_top, "dude_punch_up") != 0) return 1;
    if (loadSprite(gfx_punch_topleft,"dude_punch_upleft") != 0) return 1;
    if (loadSprite(gfx_punch_topright,"dude_punch_upright") != 0) return 1;
    if (loadSprite(gfx_punch_left,"dude_punch_left") != 0) return 1;
    if (loadSprite(gfx_punch_right,"dude_punch_right") != 0) return 1;
    if (loadSprite(gfx_punch_down,"dude_punch_down") != 0) return 1;
    if (loadSprite(gfx_punch_downleft, "dude_punch_downleft") != 0) return 1;
    if (loadSprite(gfx_punch_downright,"dude_punch_downright") != 0) return 1;
    if (loadSprite(gfx_dizzy,"dude_dizzy") != 0) return 1;
    if (loadSprite(gfx_chute,"chute") != 0) return 1;
    if (loadSprite(gfx_chute_open, "dude_chute") != 0) return 1;

    active_gfx = gfx_down;

    soundsPunch.push_back(dataStorage.getSound("sfx_punch_hit_1"));
    soundsPunch.push_back(dataStorage.getSound("sfx_punch_hit_2"));
    soundsPunch.push_back(dataStorage.getSound("sfx_punch_hit_3"));
    soundsPunch.push_back(dataStorage.getSound("sfx_punch_hit_4"));
    soundsPunch.push_back(dataStorage.getSound("sfx_punch_hit_5"));

    soundsWhiff.push_back(dataStorage.getSound("sfx_punch_whiff_1"));
    soundsWhiff.push_back(dataStorage.getSound("sfx_punch_whiff_2"));

    soundsDizzy.push_back(dataStorage.getSound("sfx_dizzy_1"));

    soundsSteal.push_back(dataStorage.getSound("sfx_steal_1"));
    soundsGrabMiss.push_back(dataStorage.getSound("sfx_grabwhiff_1"));

    return 0;
}

int Dude::display(RenderWindowPtr rwindow)
{
    if (active_gfx == nullptr)
        return 0;

    active_gfx->setPosition(position);
    rwindow->draw((*active_gfx->current_sprite));
    if (has_chute && !chute_open) {
        gfx_chute->setPosition(position);
        rwindow->draw((*gfx_chute->current_sprite));
    }

    return 0;
}

int Dude::update(const sf::Time& update_time, const sf::Time&, float& time_multiplier, std::vector<DudePtr>& dudes)
{
    acceleration.y += GRAVITY;

    if (!chute_open && moved_up == false && moved_down == false)
        max_velocity.y = TERMINAL_VELOCITY_IDLE;

    if (!isDizzy) {
        acceleration += apply_accel;
    }
    else {
        max_velocity.y = TERMINAL_VELOCITY_IDLE;
        if (update_time.asMilliseconds() > dizzyTime.asMilliseconds() + DIZZY_DURATION_MS) {
            isDizzy = false;
        }
        else {
            grabbed = false;
            punched = false;
            moved_down = false;
            moved_up = false;
            moved_left = false;
            moved_right = false;
        }
    }

    apply_accel.x = 0.f;
    apply_accel.y = 0.f;

    // Apply projectile physics on the flying entity
    if (abs(velocity.x) < ENTITY_VELOCITY_ZERO_FILTER)
        velocity.x = 0.f;
    else
    {
         if (velocity.x > 0.f)
         {
             velocity.x -= acceleration_slowdown.x;
             if (velocity.x < 0.f)
                 velocity.x = 0.f;
         }
         else if (velocity.x < 0.f)
         {
             velocity.x += acceleration_slowdown.x;
             if (velocity.x > 0.f)
                velocity.x = 0.f;
         }
    }

    if (abs(velocity.y) < ENTITY_VELOCITY_ZERO_FILTER)
        velocity.y = 0.f;
    else
    {
         if (velocity.y > 0.f)
         {
            velocity.y -= acceleration_slowdown.y;
            if (velocity.y < 0.f)
                velocity.y = 0.f;
         }
         else if (velocity.y < 0.f)
         {
             velocity.y += acceleration_slowdown.y;
             if (velocity.y > 0.f)
                 velocity.y = 0.f;
         }
    }

    if (abs(acceleration.x) < ENTITY_VELOCITY_ZERO_FILTER)
        acceleration.x = 0.f;
    if (abs(acceleration.y) < ENTITY_VELOCITY_ZERO_FILTER)
        acceleration.y = 0.f;

    // First test if the acceleration would cause too fast velocities
    sf::Vector2f new_vel = velocity;
    new_vel += acceleration * time_multiplier;

     if (abs(new_vel.x) > max_velocity.x)
         acceleration.x = 0.f;

     if (abs(new_vel.y) > max_velocity.y)
         acceleration.y = 0.f;

     velocity += acceleration * time_multiplier;

     position = position + velocity;
     if (position.x < MIN_POS_X)
         position.x = MIN_POS_X;
     else if (position.x > MAX_POS_X)
         position.x = MAX_POS_X;

     acceleration.x = 0;
     acceleration.y = 0;

     // handle punching
     if (!chute_open && punched && update_time.asMilliseconds() < punch_time.asMilliseconds() + PUNCH_COOLDOWN_MS) {
        punched = false;
     } else if (!chute_open && punched) {
         punch_time = update_time;
     }

     bool use_punch_sprite = false;
     if (update_time.asMilliseconds() < punch_time.asMilliseconds() + PUNCH_COOLDOWN_MS)
         use_punch_sprite = true;

     if (!chute_open && !isDizzy && punched) {
        bool done = false;
        DudePtr target = nullptr;
        float closest = 999999.f;

        for (auto& d : dudes) {
            if (done)
                break;

            if (d->id == id)
                continue;

            float dist = utils.dist_v2_v2(position, d->position);
            if (dist < PUNCH_DISTANCE) {
                if (dist < closest)
                    target = d;

                // prioritize the guy with the chute
                if (d->has_chute) {
                    target = d;
                    done = true;
                }
            }
        }

        if (target != nullptr) {
            target->takePunch(update_time);
            SoundPtr sfx = utils.getRandomSound(soundsPunch);
            if (sfx != nullptr)
                sfx->play();
        }
        else {
            SoundPtr sfx = utils.getRandomSound(soundsWhiff);
            if (sfx != nullptr)
                sfx->play();
        }
     } else if (!chute_open && !isDizzy && grabbed) {
         bool done = false;
         DudePtr target = nullptr;

         for (auto& d : dudes) {
             if (done)
                 break;

             if (d->id == id)
                 continue;

             // TODO: actual hitboxes around fists / opponent body
             float dist = utils.dist_v2_v2(position, d->position);
             if (dist < PUNCH_DISTANCE) {
                 if (d->has_chute) {
                     target = d;
                     done = true;
                 }
             }
         }

         if (target != nullptr) {
            if (target->isDizzy) {
                target->has_chute = false;
                has_chute = true;
                max_velocity.x = MAX_VELOCITY_X_WITH_CHUTE;
                max_velocity.y = TERMINAL_VELOCITY_HOLDING_WITH_CHUTE;
                target->max_velocity.x = MAX_VELOCITY_X;
                target->max_velocity.y = TERMINAL_VELOCITY_HOLDING;

                SoundPtr s = utils.getRandomSound(soundsSteal);
                if (s != nullptr)
                    s->play();
            } else {
                SoundPtr s = utils.getRandomSound(soundsGrabMiss);
                if (s != nullptr)
                    s->play();
            }
         }
         else {
            SoundPtr s = utils.getRandomSound(soundsGrabMiss);
            if (s != nullptr)
                s->play();
         }
     }

     if (moved_right)
         lastFacing = Right;
     else if (moved_left)
         lastFacing = Left;

     // select the right sprite based on input and current state
     if (!chute_open && isDizzy) {
        active_gfx = gfx_dizzy;
     }
     else if (!chute_open && use_punch_sprite)
     {
        if (moved_left && moved_up)
            active_gfx = gfx_punch_topleft;
        else if (moved_up && moved_right)
            active_gfx = gfx_punch_topright;
        else if (moved_up) {
            if (lastFacing == Left)
                active_gfx = gfx_punch_topleft;
            else
                active_gfx = gfx_punch_topright;
        }
        else if (moved_left && moved_down)
            active_gfx = gfx_punch_downleft;
        else if (moved_right && moved_down)
            active_gfx = gfx_punch_downright;
        else if (moved_down)
            active_gfx = gfx_punch_down;
        else if (moved_right)
            active_gfx = gfx_punch_right;
        else if (lastFacing == Left)
            active_gfx = gfx_punch_left;
        else
            active_gfx = gfx_punch_right;
     }
     else if (!chute_open)
     {
         if (moved_left && moved_up)
             active_gfx = gfx_topleft;
         else if (moved_up && moved_right)
             active_gfx = gfx_topright;
         else if (moved_up)
             if (lastFacing == Right)
                active_gfx = gfx_topright;
            else
                active_gfx = gfx_topleft;
         else if (moved_left && moved_down)
             active_gfx = gfx_downleft;
         else if (moved_right && moved_down)
             active_gfx = gfx_downright;
         else if (moved_down)
             active_gfx = gfx_down;
         else if (moved_right)
             active_gfx = gfx_right;
         else if (moved_left)
             active_gfx = gfx_left;
         else if (lastFacing == Left)
             active_gfx = gfx_left;
         else
             active_gfx = gfx_right;
     }
     else if (chute_open) {
         active_gfx = gfx_chute_open;
     }

    active_gfx->animate(update_time);
    if (active_gfx != nullptr)
        active_gfx->setPosition(position);

    moved_left = false;
    moved_right = false;
    moved_up = false;
    moved_down = false;
    punched = false;
    grabbed = false;

    return 0;
}

int Dude::punch()
{
    if (controls_disabled)
        return 0;

    punched = true;
    return 0;
}

int Dude::grab()
{
    if (controls_disabled)
        return 0;

    grabbed = true;
    return 0;
}

int Dude::openChute()
{
    max_velocity.y = TERMINAL_VELOCITY_CHUTE_OPEN;
    chute_open = true;
    return 0;
}

int Dude::takePunch(const sf::Time& update_time)
{
    punchMeter += PUNCH_POWER;
    if (!isDizzy && punchMeter > PUNCH_TOLERANCE) {
        isDizzy = true;
        dizzyTime = update_time;
        SoundPtr s = utils.getRandomSound(soundsDizzy);
        if (s != nullptr)
            s->play();
    }

    return 0;
}

int Dude::move(MoveControls m, const float& val)
{
    if (controls_disabled)
        return 0;

    switch (m)
    {
    case MoveLeft:
        apply_accel.x = ACCEL_MAX_LEFTRIGHT * val;
        if (val != 0.f)
            moved_left = true;
        break;
    case MoveRight:
        apply_accel.x = ACCEL_MAX_LEFTRIGHT * val;
        if (val != 0.f)
            moved_right = true;
        break;
    case MoveUp:
        if (val != 0.f) {
            if (has_chute)
                max_velocity.y = TERMINAL_VELOCITY_HOLDING_WITH_CHUTE;
            else
                max_velocity.y = TERMINAL_VELOCITY_HOLDING;
            moved_up = true;
        }
        break;
    case MoveDown:
        if (val != 0.f) {
            if (has_chute)
                max_velocity.y = TERMINAL_VELOCITY_DIVING_WITH_CHUTE;
            else
                max_velocity.y = TERMINAL_VELOCITY_DIVING;
            moved_down = true;
        }
        break;
    default:
        break;
    }

    return 0;
}

} // namespace
