#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include "standard_includes.h"
#include "types.h"

namespace lvt {

class DataStorage
{
public:
    DataStorage();

    int loadSound(const std::string& fpath, const std::string& saveName);
    SoundPtr getSound(const std::string& sfxName);
    int loadChute(const std::string& fpath, const std::string& savename);

    int loadMusic(const std::string& fpath, const std::string saveName);
    MusicPtr getMusic(const std::string& musicName);
    ChuteSpritePtr getSprite(const std::string& spriteName);
private:
    std::unordered_map<std::string, std::pair<SoundBufferPtr, SoundPtr > > sounds;
    std::unordered_map<std::string, MusicPtr> musics;
    std::unordered_map<std::string, ChuteSpritePtr> sprites;
};

// singleton
extern DataStorage dataStorage;

} // namespace

#endif // DATASTORAGE_H
