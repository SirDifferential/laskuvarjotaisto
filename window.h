#ifndef WINDOW_H
#define WINDOW_H

#include "standard_includes.h"
#include "types.h"
#include "scene.h"

#define NORMAL_STR_MAX 1024

namespace lvt {

typedef struct {
    char programPath[FILENAME_MAX];
    char gameVersion[NORMAL_STR_MAX];
    bool running;
    bool mustExit;
} WindowGlobals;

class Window
{
public:
    Window();

    int initWindow();
    int handleEvents();
    int readConfig();
    int updateWindow();
    int main_loop();
    int init_scene();
    EVENT control_map_to_event(CONTROL_CODE c);

    WindowGlobals globals;
private:
    CONTROL_CODE parseJoystickControl(const std::string& command);

    RenderWindowPtr pWindow;
    bool mustExit;
    ScenePtr pActiveScene;
    int active_joystick;
    std::string titleText;
    sf::VideoMode resolution;
    bool showFPS;
    int FPS;
    int windowSpawnX;
    int windowSpawnY;
    bool vsync;
    bool fullscreen;
    bool allowDangerousReso;
    std::vector<sf::VideoMode> videoModes;
    sf::VideoMode fullscreenMode;
    int wanted_fullscreen_w;
    int wanted_fullscreen_h;
    std::unordered_map<int, CONTROL_CODE> control_mapping;

    // Since analog controllers can report values near zero, ignore those values and
    // simply understand them as the joystick being at rest
    float controller_zero_tolerance;

    sf::Clock game_clock;
    sf::Time elapsed;
    std::vector<int> active_joysticks;
};

} // namespace

#endif // WINDOW_H
