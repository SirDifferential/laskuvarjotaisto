#include "utils.h"

namespace lvt
{

Utils utils;

Utils::Utils() {
    srand((unsigned int)time(NULL));
}

bool Utils::fileExists(const std::string& fpath)
{
    std::ifstream f(fpath.c_str());
    if (f.good()) {
        f.close();
        return true;
    }

    f.close();
    return false;
}


float Utils::lerp(const float& x0, const float& x1, float alpha)
{
    if (alpha > 1.0f)
        alpha = 1.0f;
    else if (alpha <= 0.f)
        alpha = 0.0f;
    float interpolation = x0 * (1.0f - alpha) + alpha * x1;

    return interpolation;
}

float Utils::dist_v2_v2(const sf::Vector2f& a, const sf::Vector2f& b) {
    float temp_x = a.x-b.x;
    float temp_y = a.y-b.y;
    return sqrtf(temp_x*temp_x + temp_y*temp_y);
}

int Utils::randomInRange(int a, int b) {
    return rand() % (b + 1 - a) + a;
}

SoundPtr Utils::getRandomSound(const std::vector<SoundPtr>& container)
{
    if (container.size() == 0) {
        fprintf(stderr, "Cannot return sound: Containre is empty\n");
        return nullptr;
    }
    if (container.size() == 1)
        return container[0];

    int selected = randomInRange(0, (int)container.size()-1);
    return container[selected];
}

} // namespace
