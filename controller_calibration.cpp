int active_joystick = -1;
const float controller_zero_tolerance = 30;

#include "standard_includes.h"
#include "types.h"
#include <iostream>

void handleEvents(lvt::RenderWindowPtr window)
{
    sf::Event event;

    while (window->pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                break;
            case sf::Event::Resized:
                break;
            case sf::Event::JoystickButtonPressed:
            {
                active_joystick = event.joystickMove.joystickId;
                std::cout << "Joystick #" << active_joystick << " button pressed: " << event.joystickButton.button << std::endl;
                break;
            }
            case sf::Event::JoystickButtonReleased:
                active_joystick = event.joystickMove.joystickId;
                std::cout << "Joystick #" << active_joystick << " button released: " << event.joystickButton.button << std::endl;
                break;
            case sf::Event::JoystickMoved:
                active_joystick = event.joystickMove.joystickId;
                std::cout << "joystick moved: " << event.joystickMove.joystickId << std::endl;
                break;
            case sf::Event::JoystickConnected:
                active_joystick = event.joystickMove.joystickId;
                std::cout << "joystick connected: " << event.joystickMove.joystickId << std::endl;
                break;
            case sf::Event::JoystickDisconnected:
                active_joystick = event.joystickMove.joystickId;
                std::cout << "joystick disconnected: " << event.joystickMove.joystickId << std::endl;
                break;
        }
    }

    if (active_joystick != -1)
    {
        // Events for joystick input
        float x_axis = sf::Joystick::getAxisPosition(active_joystick, sf::Joystick::X);
        float y_axis = sf::Joystick::getAxisPosition(active_joystick, sf::Joystick::Y);
        float z_axis = sf::Joystick::getAxisPosition(active_joystick, sf::Joystick::Z);
        float r_axis = sf::Joystick::getAxisPosition(active_joystick, sf::Joystick::R);

        if (x_axis > -controller_zero_tolerance && x_axis < controller_zero_tolerance)
            x_axis = 0;

        if (y_axis > -controller_zero_tolerance && y_axis < controller_zero_tolerance)
            y_axis = 0;

        if (z_axis > -controller_zero_tolerance && z_axis < controller_zero_tolerance)
            z_axis = 0;

        if (r_axis > -controller_zero_tolerance && r_axis < controller_zero_tolerance)
            r_axis = 0;

        if (x_axis != 0 || y_axis != 0 || z_axis != 0)
        {
            std::cout << "joystick " << active_joystick << " x:" << x_axis << " y: " << y_axis
                      << " z: " << z_axis << " r: " << r_axis << std::endl;
        }
    }
}

int main(int argc, char *argv[])
{
    lvt::RenderWindowPtr window = nullptr;
    window = lvt::RenderWindowPtr(new sf::RenderWindow(sf::VideoMode(800, 600), "controller calibration", sf::Style::Titlebar | sf::Style::Close));

    sf::Clock clock;
    sf::Time elapsed;

    sf::Time delta;
    sf::Time last_frame;
    last_frame = clock.getElapsedTime();

    while (window->isOpen())
    {
        elapsed = clock.getElapsedTime();
        delta = elapsed - last_frame;
        last_frame = clock.getElapsedTime();

        handleEvents(window);

        if (window->isOpen())
        {
            window->clear();
        }
        window->clear();

        if (window->isOpen())
        {
            window->display();
        }
    }

    return 0;
}
