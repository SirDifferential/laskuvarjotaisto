QT -= core gui

TARGET = controller_calibration
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

win32 {

    INCLUDEPATH += C:/SFML-2.5.0/include

    # SFML gets real mad if release libs are used in debug builds
    CONFIG(debug, debug|release) {
        LIBS += -L"C:/SFML-2.5.0/build/lib/Debug" -lsfml-main-d -lsfml-system-d -lsfml-window-d -lsfml-graphics-d
    }

    CONFIG(release, debug|release) {
        LIBS += -L"C:/SFML-2.5.0/build/lib/RelWithDebInfo" -lsfml-main -lsfml-system -lsfml-window -lsfml-graphics
    }
}

macx {
    QMAKE_CXXFLAGS += -std=c++11
    QMAKE_CXXFLAGS += -Werror # warnings as errors
    QMAKE_LFLAGS += -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system
}

linux {
    QMAKE_CXXFLAGS += -std=c++11
    QMAKE_CXXFLAGS += -Werror # warnings as errors
    QMAKE_CXXFLAGS += -Wno-switch # Ignore "Warning: enumeration value ‘Count’ not handled in switch [-Wswitch]"
    QMAKE_LFLAGS += -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system
}

SOURCES += controller_calibration.cpp
