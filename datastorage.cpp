#include "datastorage.h"
#include "chutesprite.h"

namespace lvt {

DataStorage dataStorage;

DataStorage::DataStorage()
{

}

int DataStorage::loadSound(const std::string& fpath, const std::string& saveName)
{
    SoundBufferPtr buf(new sf::SoundBuffer());
    if (buf->loadFromFile(fpath) == false) {
        fprintf(stderr, "Failed loading sound: %s\n", fpath.c_str());
        return 1;
    }

    SoundPtr s(new sf::Sound());
    s->setBuffer((*buf));
    std::pair<SoundBufferPtr, SoundPtr> p;
    p.first = buf;
    p.second = s;
    sounds[saveName] = p;
    return 0;
}

SoundPtr DataStorage::getSound(const std::string& sfxName)
{
    if (sounds.find(sfxName) == sounds.end())
        return nullptr;
    return sounds[sfxName].second;
}

int DataStorage::loadChute(const std::string& fpath, const std::string& savename) {
    ChuteSpritePtr s(new ChuteSprite());
    if (s->loadFromChute(fpath) != 0) {
        fprintf(stderr, "Failed loading chute: %s\n", fpath.c_str());
        return 1;
    }

    sprites[savename] = s;
    return 0;
}

int DataStorage::loadMusic(const std::string& fpath, const std::string saveName)
{
    MusicPtr m(new sf::Music());
    if (m->openFromFile(fpath) == false) {
        fprintf(stderr, "Failed loading music: %s\n", fpath.c_str());
        return 1;
    }

    musics[saveName] = m;
    return 0;
}

MusicPtr DataStorage::getMusic(const std::string& musicName)
{
    if (musics.find(musicName) == musics.end())
        return nullptr;
    return musics[musicName];
}

ChuteSpritePtr DataStorage::getSprite(const std::string& spriteName)
{
    if (sprites.find(spriteName) == sprites.end())
        return nullptr;
    return sprites[spriteName];
}

} // namespace
