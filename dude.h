#ifndef DUDE_H
#define DUDE_H

#include "types.h"
#include "standard_includes.h"

const float GRAVITY = 90.81f;
const float ACCEL_MAX_LEFTRIGHT = 25.f;
const float ACCEL_MAX_UP = 30.f;
const float ACCEL_MAX_DOWN = 30.f;
const float ENTITY_VELOCITY_ZERO_FILTER = 0.025f;
const float MIN_POS_X = -999999.f;
const float MAX_POS_X = 99999.f;
const float PUNCH_DISTANCE = 130.f;
const float PUNCH_POWER = 20.f;
const float PUNCH_TOLERANCE = 100.f;
const int DIZZY_DURATION_MS = 3500;
const int PUNCH_COOLDOWN_MS = 500;

const float TERMINAL_VELOCITY_DIVING = 10.f;

// This is the same for guys with the chute and those without it
const float TERMINAL_VELOCITY_IDLE = 8.f;
const float TERMINAL_VELOCITY_HOLDING = 7.f;

const float MAX_VELOCITY_X = 4.f;
const float MAX_VELOCITY_X_WITH_CHUTE = 2.f;

// Allows other players to catch up to this person
const float TERMINAL_VELOCITY_DIVING_WITH_CHUTE = 9.f;
const float TERMINAL_VELOCITY_HOLDING_WITH_CHUTE = 7.5f;
const float TERMINAL_VELOCITY_CHUTE_OPEN = 1.f;

namespace lvt {

typedef enum {
    MoveLeft = 0,
    MoveRight = 1,
    MoveUp = 2,
    MoveDown = 3
} MoveControls;

typedef enum {
    Left = 0,
    Right = 1
} Facing;

class Dude
{
public:
    Dude(int p_id);

    int id;
    bool has_chute;

    sf::Vector2f position;
    sf::Vector2f velocity;
    sf::Vector2f acceleration;
    sf::Vector2f acceleration_slowdown;
    sf::Vector2f max_velocity;
    sf::Vector2f apply_accel;

    bool controls_disabled;

    // Flags used to select the correct sprite based on this frame's input
    bool moved_left;
    bool moved_right;
    bool moved_up;
    bool moved_down;
    bool punched;
    bool grabbed;
    bool chute_open;

    // Flag for selecting if left or right facing sprite is shown in absence of input
    Facing lastFacing;

    sf::Time punch_time;
    float punchMeter;
    bool isDizzy;
    sf::Time dizzyTime;

    /**
     * @param update_time: The current process time since start
     * @param delta: The time since last call to this function
     * @param time_multiplier: Float between 0 and 1, depending how close to 1000 milliseconds delta is
     * @param dudes: Vector of all dudes in the scene
     */
    int update(const sf::Time& update_time, const sf::Time&, float& time_multiplier, std::vector<DudePtr>& dudes);
    int display(RenderWindowPtr rwindow);
    int init();
    int move(MoveControls m, const float& val);
    int punch();
    int grab();
    int takePunch(const sf::Time& update_time);
    int openChute();

    ChuteSpritePtr gfx_top;
    ChuteSpritePtr gfx_topleft;
    ChuteSpritePtr gfx_topright;
    ChuteSpritePtr gfx_left;
    ChuteSpritePtr gfx_right;
    ChuteSpritePtr gfx_down;
    ChuteSpritePtr gfx_downleft;
    ChuteSpritePtr gfx_downright;

    ChuteSpritePtr gfx_punch_top;
    ChuteSpritePtr gfx_punch_topleft;
    ChuteSpritePtr gfx_punch_topright;
    ChuteSpritePtr gfx_punch_left;
    ChuteSpritePtr gfx_punch_right;
    ChuteSpritePtr gfx_punch_down;
    ChuteSpritePtr gfx_punch_downleft;
    ChuteSpritePtr gfx_punch_downright;

    ChuteSpritePtr gfx_dizzy;

    ChuteSpritePtr gfx_chute;
    ChuteSpritePtr gfx_chute_open;

    // Assigned to whatever needs to be currently shown
    // This must never be nullptr after the Dude has been initted via init()
    ChuteSpritePtr active_gfx;

    std::vector<SoundPtr> soundsPunch;
    std::vector<SoundPtr> soundsWhiff;
    std::vector<SoundPtr> soundsDizzy;
    std::vector<SoundPtr> soundsSteal;
    std::vector<SoundPtr> soundsGrabMiss;
private:
    int loadSprite(ChuteSpritePtr& ptr, const std::string& name);
};

} // namespace

#endif // DUDE_H
