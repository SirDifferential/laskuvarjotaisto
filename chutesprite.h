#ifndef CHUTESPRITE_H
#define CHUTESPRITE_H

#include "standard_includes.h"
#include "types.h"

namespace lvt
{

class ChuteSprite
{
public:
    ChuteSprite();

    /**
     * @brief Loads an animation sprite sequence from .chute file
     */
    int loadFromChute(const std::string& fpath);
    void setPosition(const sf::Vector2f& v);
    void animate(const sf::Time& update_time);

    int loadFromFileLooping(const std::string& fpath, const sf::IntRect& rect);
    int loadFromFile(const std::string& fpath);

    std::vector<SpritePtr> sprites;
    std::vector<TexturePtr> textures;
    std::vector<std::string> fpaths;
    SpritePtr current_sprite;
    int frames;
    int currentFrame;
    int anim_speed_ms;
    sf::Time anim_time;
private:
    SpritePtr loadSpriteFromFile(const std::string &fname, TexturePtr& dest);
    TexturePtr loadTextureFromFile(const std::string &fname);
};

} // namespace

#endif // CHUTESPRITE_H
