#include "scene.h"
#include "dude.h"
#include "utils.h"
#include "chutesprite.h"
#include "datastorage.h"

const std::string font_fpath = "./gfx/font/Gravity-Regular.otf";
const std::string fpath_menubg = "./gfx/clouds-above-the-sky-1.jpg";
const std::string BG_FPATH = "./gfx/clouds-background-003.jpg";
const std::string MARKER_FPATH = "./gfx/marker.png";
const std::string controls_fpath = "./gfx/controls.png";

const std::string fpath_meter = "./gfx/meter.chute";
const std::string fpath_meter_cursor = "./gfx/meter_cursor.chute";

const std::string fpath_music_charselect = "./sound/IncompetechInc/CyborgNinja.ogg";
const std::string fpath_music_ingame = "./sound/IncompetechInc/TheWhip.ogg";

const std::string fpath_punch1 = "./sound/freesound/punch1.ogg";
const std::string fpath_punch2 = "./sound/freesound/punch2.ogg";
const std::string fpath_punch3 = "./sound/freesound/punch3.ogg";
const std::string fpath_punch4 = "./sound/freesound/punch4.ogg";
const std::string fpath_punch5 = "./sound/freesound/punch5.ogg";

const std::string fpath_whiff1 = "./sound/freesound/whiff1.ogg";
const std::string fpath_whiff2 = "./sound/freesound/whiff2.ogg";

const std::string fpath_dizzy1 = "./sound/freesound/dizzy1.ogg";

const std::string fpath_steal1 = "./sound/freesound/steal.ogg";
const std::string fpath_grabwhiff = "./sound/freesound/grabwhiff.ogg";

const std::string fpath_bomb = "./sound/freesound/bomb.ogg";
const std::string fpath_blort = "./sound/freesound/blort.ogg";

// Character sprites
const std::string gfx_dude_normal_up = "./gfx/dude_normal_up.chute";
const std::string gfx_dude_normal_up_right = "./gfx/dude_normal_upright.chute";
const std::string gfx_dude_normal_up_left = "./gfx/dude_normal_upleft.chute";
const std::string gfx_dude_normal_right = "./gfx/dude_normal_right.chute";
const std::string gfx_dude_normal_left = "./gfx/dude_normal_left.chute";
const std::string gfx_dude_normal_down = "./gfx/dude_normal_down.chute";
const std::string gfx_dude_normal_downright = "./gfx/dude_normal_downright.chute";
const std::string gfx_dude_normal_downleft = "./gfx/dude_normal_downleft.chute";

const std::string gfx_dude_punch_up = "./gfx/dude_punch_up.chute";
const std::string gfx_dude_punch_upright = "./gfx/dude_punch_upright.chute";
const std::string gfx_dude_punch_upleft = "./gfx/dude_punch_upleft.chute";
const std::string gfx_dude_punch_right = "./gfx/dude_punch_right.chute";
const std::string gfx_dude_punch_left = "./gfx/dude_punch_left.chute";
const std::string gfx_dude_punch_down = "./gfx/dude_punch_down.chute";
const std::string gfx_dude_punch_downright = "./gfx/dude_punch_downright.chute";
const std::string gfx_dude_punch_downleft = "./gfx/dude_punch_downleft.chute";

const std::string gfx_dude_chuteopen = "./gfx/dude_chute.chute";

const std::string gfx_chute_fpath = "./gfx/chute.chute";

// TODO
const std::string gfx_dude_dizzy = "./gfx/dude_normal_upright.chute";

const float DUDE_SPAWN_Y = 500.f;

namespace lvt {

Scene::Scene(RenderWindowPtr rwindow)
{
    must_exit = false;
    auto s = rwindow->getSize();
    view.reset(sf::FloatRect(0.f, 0.f, (float)s.x, (float)s.y));

    // setViewport with (0,0,1,1) for (width,height) window means we get the right aspect ratio (no bad stretching)
    view.setViewport(sf::FloatRect(0.f, 0.f, 1.0f, 1.0f));
    lookat.x = 0;
    lookat.y = 0;
    zoom_level = 4.f;

    scenestate = WaitPlayers;

    controls_img = ChuteSpritePtr(new ChuteSprite());
    menu_bg = ChuteSpritePtr(new ChuteSprite());
    bg = ChuteSpritePtr(new ChuteSprite());
    marker = ChuteSpritePtr(new ChuteSprite());
    meter = ChuteSpritePtr(new ChuteSprite());
    meter_cursor = ChuteSpritePtr(new ChuteSprite());
    scene_length_ms = 50000;
    level_percent = 0.f;

    bomb_sfx_duration_ms = 6200;
    wait_until_lobby_ms = 10000;
    blort_played = false;
    is_loading = true;
}

int Scene::addDude(int id)
{
    DudePtr d(new Dude(id));

    dudes.push_back(d);

    if (d->init() != 0) {
        return 1;
    }

    d->position.x = id * 300.f;
    d->position.y = DUDE_SPAWN_Y;

    return 0;
}

int Scene::drawLoading(RenderWindowPtr rwindow, const std::string& text) {
    if (!rwindow->isOpen())
        return 1;

    rwindow->clear();
    display(rwindow);
    sf::Text t(text, (*font));
    t.setCharacterSize(30);
    t.setFillColor(sf::Color::White);
    t.setPosition(260, 300);
    rwindow->draw(t);
    rwindow->display();

    return 0;
}

int Scene::init(RenderWindowPtr rwindow)
{
    is_loading = true;

    if (dataStorage.loadMusic(fpath_music_charselect, "music_charselect") != 0)
        return 1;

    music_charselect = dataStorage.getMusic("music_charselect");

    if (music_charselect == nullptr) {
        fprintf(stderr, "charselect music is null\n");
        return 1;
    }

    music_charselect->play();

    font = FontPtr(new sf::Font());
    if (!font->loadFromFile(font_fpath)) {
        fprintf(stderr, "loading font failed: %s\n", font_fpath.c_str());
        return 1;
    }

    if (menu_bg->loadFromFile(fpath_menubg) != 0) {
        fprintf(stderr, "failed loading menu bg: %s\n", fpath_menubg.c_str());
        return 1;
    }

    if (controls_img->loadFromFile(controls_fpath) != 0) {
        fprintf(stderr, "Failed loading controls image: %s\n", controls_fpath.c_str());
        return 1;
    }

    // Surely different resolutions will never be a problem
    controls_img->current_sprite->setPosition(500, 350);

    if (drawLoading(rwindow, "Loading music") != 0)
        return 1;

    sf::IntRect rect(0, 0, 5000, 5000);
    if (bg->loadFromFileLooping(BG_FPATH, rect) != 0)
        return 1;

    bg->current_sprite->setPosition(-50000, -2000);
    bg->current_sprite->setScale(35.f, 35.f);

    if (marker->loadFromFile(MARKER_FPATH) != 0)
        return 1;

    if (dataStorage.loadMusic(fpath_music_ingame, "music_ingame") != 0)
        return 1;

    if (drawLoading(rwindow, "Loading sounds") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_punch1, "sfx_punch_hit_1") != 0)
        return 1;
    if (dataStorage.loadSound(fpath_punch2, "sfx_punch_hit_2") != 0)
        return 1;
    if (dataStorage.loadSound(fpath_punch3, "sfx_punch_hit_3") != 0)
        return 1;
    if (dataStorage.loadSound(fpath_punch4, "sfx_punch_hit_4") != 0)
        return 1;
    if (dataStorage.loadSound(fpath_punch5, "sfx_punch_hit_5") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_whiff1, "sfx_punch_whiff_1") != 0)
        return 1;
    if (dataStorage.loadSound(fpath_whiff2, "sfx_punch_whiff_2") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_dizzy1, "sfx_dizzy_1") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_steal1, "sfx_steal_1") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_grabwhiff, "sfx_grabwhiff_1") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_music_ingame, "sfx_punch_whiff") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_bomb, "sfx_bomb") != 0)
        return 1;

    if (dataStorage.loadSound(fpath_blort, "sfx_blort") != 0)
        return 1;

    music_ingame = dataStorage.getMusic("music_ingame");
    if (music_ingame == nullptr) {
        fprintf(stderr, "ingame music is null\n");
        return 1;
    }

    if (drawLoading(rwindow, "Loading graphics") != 0)
        return 1;

    if (meter->loadFromChute(fpath_meter) != 0)
        return 1;
    if (meter_cursor->loadFromChute(fpath_meter_cursor) != 0)
        return 1;

    if (dataStorage.loadChute(gfx_dude_normal_up, "dude_normal_up") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_up_left, "dude_normal_upleft") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_up_right, "dude_normal_upright") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_left, "dude_normal_left") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_right, "dude_normal_right") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_downleft, "dude_normal_downleft") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_downright, "dude_normal_downright") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_down, "dude_normal_down") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_up, "dude_punch_up") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_upleft, "dude_punch_upleft") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_upright, "dude_punch_upright") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_left, "dude_punch_left") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_right, "dude_punch_right") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_downleft, "dude_punch_downleft") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_downright, "dude_punch_downright") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_punch_down, "dude_punch_down") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_chuteopen, "dude_chute") != 0) return 1;
    if (dataStorage.loadChute(gfx_dude_normal_up_left, "dude_dizzy") != 0) return 1;
    if (dataStorage.loadChute(gfx_chute_fpath, "chute") != 0) return 1;

    // Too lazy to render again at right size -_-
    ChuteSpritePtr s = dataStorage.getSprite("dude_chute");
    for (auto& sp : s->sprites) {
        sp->setScale(1.5f, 1.5f);
    }

    menu_text = TextPtr(new sf::Text());
    menu_text->setFont((*font));
    menu_text->setString("Press start on your controller to join. Press A or X to start.\n");
    menu_text->setFillColor(sf::Color::White);
    menu_text->setPosition(260, 300);

    is_loading = false;
    return 0;
}

int Scene::event(const sf::Time& event_time, const int& controller_id, const EVENT& code)
{
    return event(event_time, controller_id, code, 0.0f);
}

int Scene::event(const sf::Time& event_time, const int& controller_id, const EVENT &code, const float& data)
{
    if (code == EVENT_ESCAPE) {
        must_exit = true;
        return 0;
    }

    if (scenestate == WaitPlayers)
    {
        switch( code)
        {
        case EVENT_ESCAPE:
            must_exit = true;
            break;
        case EVENT_ENTER:
        {
            // Add a new dude and remember which joystick controls it
            if (control_target.find(controller_id) == control_target.end())
            {
                if (addDude((int)dudes.size()+1) != 0) {
                    must_exit = true;
                    return 1;
                }
                control_target[controller_id] = dudes.at(dudes.size()-1);
            }
            break;
        }
        case EVENT_Z:
        case EVENT_X:
        case EVENT_C:
            if (control_target.size() > 0) {
                scenestate = InGame;
                music_charselect->stop();
                music_ingame->setVolume(50.f);
                music_ingame->play();

                // Give chute to random dude
                int give_chute_to = utils.randomInRange(0, (int)dudes.size()-1);
                dudes[give_chute_to]->has_chute = true;
                dudes[give_chute_to]->max_velocity.x = MAX_VELOCITY_X_WITH_CHUTE;
                dudes[give_chute_to]->max_velocity.y = TERMINAL_VELOCITY_HOLDING_WITH_CHUTE;

                scene_start_time = event_time;

                for (auto& d : dudes) {
                    d->controls_disabled = false;
                }
            }
            break;
        }

        return 0;
    }

    DudePtr target = nullptr;
    if (control_target.find(controller_id) != control_target.end())
        target = control_target[controller_id];

    if (target == nullptr)
        return 0;

    switch (code)
    {
    case EVENT_UP:
        break;
    case EVENT_DOWN:
        break;
    case EVENT_ENTER:
        break;
    case EVENT_SHIFT:
        break;
    case EVENT_Z:
        target->punch();
        break;
    case EVENT_X:
        target->grab();
        break;
    case EVENT_C:
        break;
    case EVENT_LEFT_IS_PRESSED:
        target->move(MoveLeft, data);
        break;
    case EVENT_RIGHT_IS_PRESSED:
        target->move(MoveRight, data);
        break;
    case EVENT_UP_IS_PRESSED:
        target->move(MoveUp, data);
        break;
    case EVENT_DOWN_IS_PRESSED:
        target->move(MoveDown, data);
        break;
    case EVENT_JOYSTICK_X_AXIS:
        if (data < 0)
            target->move(MoveLeft, data);
        else
            target->move(MoveRight, data);
        break;
    case EVENT_JOYSTICK_Y_AXIS:
        if (data < 0)
            target->move(MoveUp, data);
        else
            target->move(MoveDown, data);
        break;
    case EVENT_JOYSTICK_Z_AXIS:
        break;
    case EVENT_JOYSTICK_R_AXIS:
        break;
    case EVENT_UNKNOWN:
        break;
    }

    return 0;
}

int Scene::update(const sf::Time& update_time, const sf::Time& delta)
{
    // Compute a multiplier to be used for all physics calculations that use "one second" as their unit
    // This multiplier defines by what they must be multiplied to apply for milliseconds
    // In range [0,1], reaching 1 if delta time is 1000 milliseconds
    float time_multiplier = utils.lerp(0.f, 1.f, (float)delta.asMilliseconds() / 1000.f);

    if (scenestate == InGame)
    {
        for (auto& d : dudes) {
            if (d->update(update_time, delta, time_multiplier, dudes) != 0) {
                fprintf(stderr, "Failed updating entity: %d\n", d->id);
                return 1;
            }
        }

        level_percent = update_time.asMilliseconds() / (float)(scene_start_time.asMilliseconds() + scene_length_ms);

        if (update_time.asMilliseconds() > scene_start_time.asMilliseconds() + scene_length_ms) {
            for (auto& d : dudes) {
                d->controls_disabled = true;

                if (d->has_chute == true) {
                    d->openChute();
                }
            }

            scenestate = ChuteOpen;

            SoundPtr s = dataStorage.getSound("sfx_bomb");
            if (s != nullptr)
                s->play();

            chute_open_time = update_time;
        }
    }
    else if (scenestate == ChuteOpen)
    {
        for (auto& d : dudes) {
            if (d->update(update_time, delta, time_multiplier, dudes) != 0) {
                fprintf(stderr, "Failed updating entity: %d\n", d->id);
                return 1;
            }
        }

        if (!blort_played && update_time.asMilliseconds() > chute_open_time.asMilliseconds() + bomb_sfx_duration_ms) {
            SoundPtr s = dataStorage.getSound("sfx_blort");
            if (s != nullptr)
                s->play();
            blort_played = true;
        }

        if (update_time.asMilliseconds() > chute_open_time.asMilliseconds() + wait_until_lobby_ms) {
            scenestate = WaitPlayers;

            dudes.clear();
            control_target.clear();
            lookat.x = 0;
            lookat.y = 0;
            zoom_level = 1.f;
            level_percent = 0.f;
            blort_played = false;
            music_ingame->stop();
            music_charselect->play();
        }
    }

    return 0;
}

int Scene::display(RenderWindowPtr rwindow)
{
    if (scenestate == WaitPlayers)
    {
        auto s = rwindow->getSize();
        view.reset(sf::FloatRect(0.f, 0.f, (float)s.x, (float)s.y));
        view.zoom(1.f);
        rwindow->setView(view);
        rwindow->draw((*menu_bg->current_sprite));
        rwindow->draw((*controls_img->current_sprite));

        if (!is_loading) {
            rwindow->draw((*menu_text));
        }

        for (auto& d : dudes) {
            d->display(rwindow);
        }

        return 0;
    }

    if (dudes.size() == 0) {
        fprintf(stderr, "cannot render scene with 0 dudes\n");
        return 1;
    }

    sf::Vector2f s = view.getSize();

    sf::Vector2f sum_pos;
    sum_pos.x = 0;
    sum_pos.y = 0;

    sf::Vector2f min_coord;
    sf::Vector2f max_coord;

    min_coord.x = dudes[0]->position.x;
    min_coord.y = dudes[0]->position.y;
    max_coord.x = dudes[0]->position.x;
    max_coord.y = dudes[0]->position.y;

    for (const auto& d : dudes)
    {
        sum_pos += d->position;
        if (d->position.x < min_coord.x)
            min_coord.x = d->position.x;
        if (d->position.y < min_coord.y)
            min_coord.y = d->position.y;
        if (d->position.x > max_coord.x)
            max_coord.x = d->position.x;
        if (d->position.y > max_coord.y)
            max_coord.y = d->position.y;
    }

    sf::Vector2f mid;
    mid.x = sum_pos.x / dudes.size();
    mid.y = sum_pos.y / dudes.size();

    if (scenestate == ChuteOpen) {
        DudePtr target = nullptr;

        for (auto& d : dudes) {
            if (d->has_chute) {
                target = d;
                break;
            }
        }

        if (target != nullptr)
            mid = target->position;
    }

    sf::Vector2f largest_dist;
    largest_dist.x = max_coord.x - min_coord.x;
    largest_dist.y = max_coord.y - min_coord.y;
    float largest = std::max(largest_dist.x, largest_dist.y);

    // Apply a tiny smoothing to camera scrolling
    sf::Vector2f lookat_dest = mid;
    sf::Vector2f delta = lookat_dest - lookat;

    delta.x *= 0.3f;
    delta.y *= 0.3f;

    if (abs(delta.x) > 0.1f)
        lookat.x += delta.x;
    if (abs(delta.y) > 0.1f)
        lookat.y += delta.y;

    marker->setPosition(lookat);

    // Set the camera to look at the player
    sf::FloatRect rect(lookat.x, lookat.y, s.x, s.y);
    view.reset(rect);
    view.setCenter(lookat.x, lookat.y);

    float zoom_level_wanted = utils.lerp(1.5f, 10.f, largest / 8000.0f);
    if (scenestate == ChuteOpen) {
        zoom_level_wanted = 1.5f;
    }

    // Apply a tiny smoothing to zooming
    float zoom_delta = zoom_level_wanted - zoom_level;

    zoom_delta *= 0.005f;

    if (abs(zoom_delta) > 0.001f)
        zoom_level += zoom_delta;

    view.setSize(rwindow->getSize().x * zoom_level, rwindow->getSize().y * zoom_level);

    rwindow->draw((*bg->current_sprite));

    for (auto& d : dudes)
    {
        d->display(rwindow);
    }

    sf::Vector2f meter_pos = lookat;
    meter_pos.x += view.getSize().x * 0.4f;

    meter->current_sprite->setPosition(meter_pos);

    float cursor_start_y = -230.f;
    float cursor_end_y = 230.f;

    float cursor_add = utils.lerp(cursor_start_y, cursor_end_y, level_percent);

    meter_pos.y += cursor_add;
    meter_pos.x -= 1.f;
    meter_cursor->current_sprite->setPosition(meter_pos);

    rwindow->draw((*meter->current_sprite));
    rwindow->draw((*meter_cursor->current_sprite));

    rwindow->setView(view);

    return 0;
}

} // namespace
