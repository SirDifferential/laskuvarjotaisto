#include "window.h"
#include "standard_includes.h"

#ifdef WIN32
#include <Windows.h>
// Request dedicated GPU on systems that have it
extern "C" { __declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001; }
extern "C" { __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1; }
#endif

int main(int argc, char** argv)
{
    std::string dir = argv[0];

    // Get directory from the full filepath
    for (int i = static_cast<int>(dir.size())-1; i > 0; i--)
    {
        if (dir.at(i) == '/' || dir.at(i) == '\\')
        {
            break;
        }
        dir.erase(i, 1);
    }

    lvt::Window window;

    strcpy(window.globals.programPath, dir.c_str());

    if (window.readConfig() != 0) {
        return 1;
    }

    if (window.initWindow() != 0) {
        return 1;
    }

    if (window.init_scene() != 0) {
        return 1;
    }

    return window.main_loop();
}
