# Laskuvarjotaisto

Licensed: MIT

Font: https://www.dafont.com/gravity.font?l[]=10

Music from: https://incompetech.com/music/royalty-free/music.html

Sound effects from: https://freesound.org/
* https://freesound.org/people/RSilveira_88/sounds/216196/ licensed CC RSilveira_880
* https://freesound.org/people/CastIronCarousel/sounds/216782/ licensed CC CastIronCarousel
* https://freesound.org/people/Huminaatio/sounds/390462/ licensed CC Zero Huminaatio
* https://freesound.org/people/nextmaking/sounds/86019/ licensed Sampling+ nextmaking
* https://freesound.org/people/rcroller/sounds/424144/ licensed CC Zero rcroller
* https://freesound.org/people/deleted_user_6479820/sounds/353046/ CC Zero deleted_user_6479820
* https://freesound.org/people/deleted_user_6479820/sounds/353048/ CC Zero deleted_user_6479820
* https://freesound.org/people/AlineAudio/sounds/416838/ CC Zero AlineAudio
* https://freesound.org/people/Dpoggioli/sounds/263541/ CC Attribution Dpoggioli
* https://freesound.org/people/FairhavenCollection/sounds/372899/ CC Zero FairhavenCollection
* https://freesound.org/people/morganpurkis/sounds/398255/ CC Zero morganpurkis
* https://freesound.org/people/timtube/sounds/61046/ CC NC timtube

MakeHuman accessories
* http://www.makehumancommunity.org/clothes/motorcycle_jacket.html licensed CC punkduck
* http://www.makehumancommunity.org/clothes/motorcycle_pants.html licensed CC punkduck 

Sky photos:
* https://www.publicdomainpictures.net/en/view-image.php?image=50608&picture=clouds-above-the-sky-1 CC0 Maliz Ong

* Xbox 360 controller photo https://commons.wikimedia.org/wiki/File:Xbox-360-Controller-Black.png


### Building

Install dependencies:

* C++11 compiler
* SFML version > 2.1

#### CMake

```
cd repodir
mkdir build && cd build
cmake ..
make
cp -r ../config.json ../sound ../gfx ./
./laskuvarjotaisto
```

#### QMake

```
cd repodir
qmake laskuvarjotaisto.pro
make
./laskuvarjotaisto
```
