#ifndef UTILS_H
#define UTILS_H

#include <fstream>
#include "types.h"
#include "standard_includes.h"

namespace lvt
{

class Utils {
public:
    Utils();

    bool fileExists(const std::string& fpath);
    SpritePtr loadSpriteFromFile(const std::string &fname, TexturePtr& dest);
    float lerp(const float& x0, const float& x1, float alpha);
    TexturePtr loadTextureFromFile(const std::string &fname);
    float dist_v2_v2(const sf::Vector2f& a, const sf::Vector2f& b);
    int randomInRange(int a, int b);
    SoundPtr getRandomSound(const std::vector<SoundPtr>& container);
};

// Singleton
extern Utils utils;

} // namespace

#endif // UTILS_H
