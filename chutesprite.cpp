#include "chutesprite.h"

namespace lvt
{

ChuteSprite::ChuteSprite()
{
    frames = 0;
    currentFrame = 0;
    anim_speed_ms = 33;
}

TexturePtr ChuteSprite::loadTextureFromFile(const std::string &fname)
{
    TexturePtr dest = TexturePtr(new sf::Texture());

    if (!dest->loadFromFile(fname)) {
        fprintf(stderr, "Failed loading %s from file\n", fname.c_str());
        return nullptr;
    }
    return dest;
}

SpritePtr ChuteSprite::loadSpriteFromFile(const std::string &fname, TexturePtr& dest)
{
    dest = TexturePtr(new sf::Texture());

    if (!dest->loadFromFile(fname)) {
        fprintf(stderr, "Failed loading %s from file\n", fname.c_str());
        return nullptr;
    }

    SpritePtr dest_spr(new sf::Sprite());
    dest_spr->setTexture((*dest));

    return dest_spr;
}

int ChuteSprite::loadFromFile(const std::string& fpath)
{
    TexturePtr tex = loadTextureFromFile(fpath);
    if (tex == nullptr) {
        fprintf(stderr, "Failed loading file as texture: %s\n", fpath.c_str());
        return 1;
    }

    SpritePtr s = SpritePtr(new sf::Sprite((*tex)));
    current_sprite = s;
    frames = 1;
    textures.push_back(tex);
    sprites.push_back(s);

    return 0;
}

int ChuteSprite::loadFromFileLooping(const std::string& fpath, const sf::IntRect& rect)
{
    TexturePtr tex = loadTextureFromFile(fpath);
    if (tex == nullptr) {
        fprintf(stderr, "Failed loading file as texture: %s\n", fpath.c_str());
        return 1;
    }

    tex->setRepeated(true);
    SpritePtr s = SpritePtr(new sf::Sprite((*tex), rect));
    current_sprite = s;
    frames = 1;
    textures.push_back(tex);
    sprites.push_back(s);

    return 0;
}

int ChuteSprite::loadFromChute(const std::string &fpath)
{
    std::ifstream in(fpath);
    if (!in.good()) {
        fprintf(stderr, "Failed reading file: %s\n", fpath.c_str());
        return 1;
    }

    std::string l;
    int line = 0;
    while (std::getline(in, l))
    {
        if (line == 0) {
            if (strncmp(l.c_str(), "ms_per_frame=", 13) != 0) {
                fprintf(stderr, "No ms_per_frame= at the beginning of the .chute file %s\n", fpath.c_str());
                return 1;
            }

            std::string ms = l.substr(13, l.size());
            int ms_int = 0;
            try {
                ms_int = std::stoi(ms, NULL, 10);
            } catch (const std::exception& e) {
                fprintf(stderr, "Failed converting animation speed to ms: %s\n", e.what());
                return 1;
            }

            anim_speed_ms = ms_int;
        } else {
            TexturePtr tex;
            SpritePtr s = loadSpriteFromFile(l, tex);
            if (s == nullptr) {
                fprintf(stderr, "Failed loading sprite from file: %s\n", l.c_str());
                return 1;
            }

            // Center all sprites
            sf::FloatRect b = s->getGlobalBounds();
            s->setOrigin(b.width / 2.f, b.height / 2.f);

            sprites.push_back(s);
            textures.push_back(tex);
            frames++;
        }

        line++;
    }

    if (sprites.size() != textures.size() || sprites.size() == 0) {
        fprintf(stderr, "Did not load sprites or textures from .chute file: %s\n", fpath.c_str());
        return 1;
    }

    current_sprite = sprites.at(0);

    fprintf(stderr, "Loaded animation with %d frames from %s\n", frames, fpath.c_str());

    return 0;
}

void ChuteSprite::setPosition(const sf::Vector2f& v)
{
    if (current_sprite == nullptr)
        return;

    current_sprite->setPosition(v);
}

void ChuteSprite::animate(const sf::Time& update_time) {
    if (update_time.asMilliseconds() > anim_time.asMilliseconds() + anim_speed_ms) {
        if (sprites.size() == 0)
            current_sprite = sprites.at(0);
        else {
            currentFrame++;
            if (currentFrame >= sprites.size())
                currentFrame = 0;
            current_sprite = sprites.at(currentFrame);
        }

        anim_time = update_time;
    }
}

} // namespace
