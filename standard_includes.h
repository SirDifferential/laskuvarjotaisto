#ifndef STANDARD_INCLUDES_H
#define STANDARD_INCLUDES_H

#include <memory>
#include <vector>
#include <list>
#include <unordered_map>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <random>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics/Text.hpp>

#endif // STANDARD_INCLUDES_H
