TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    window.cpp \
    scene.cpp \
    jsoncpp.cpp \
    utils.cpp \
    dude.cpp \
    chutesprite.cpp \
    datastorage.cpp

win32 {
    INCLUDEPATH += C:/SFML-2.5.0/include

    # SFML gets real mad if release libs are used in debug builds
    CONFIG(debug, debug|release) {
        LIBS += -L"C:/SFML-2.5.0/build/lib/Debug" -lsfml-main-d -lsfml-system-d -lsfml-window-d -lsfml-audio-d -lsfml-graphics-d
    }

    CONFIG(release, debug|release) {
        LIBS += -L"C:/SFML-2.5.0/build/lib/RelWithDebInfo" -lsfml-main -lsfml-system -lsfml-window -lsfml-audio -lsfml-graphics
    }

    QMAKE_CXXFLAGS += /W3
}

linux {
    LIBS += -lsfml-main -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio
    QMAKE_CXXFLAGS += -Wall -std=c++11
}

HEADERS += \
    window.h \
    types.h \
    scene.h \
    scene.h \
    utils.h \
    standard_includes.h \
    dude.h \
    chutesprite.h \
    datastorage.h
