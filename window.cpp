#include "window.h"
#include "json/json.h"
#include "utils.h"

#define VERIFY_WINDOW(w) { if (w == nullptr || !w->isOpen()) return 1; }

// Maps to the first joystick
#define CONTROLLER_ID_KEYBOARD 1
#define INPUT_MAX_VAL 100.0f
#define CLAMP(val, min, max) { if (val <= min) val = min; if (val >= max) val = max; }

namespace lvt {

Window::Window()
{
    pWindow = nullptr;
    mustExit = false;
    pActiveScene = nullptr;
    active_joystick = -1;
    memset(&globals, 0, sizeof(globals));
    titleText = "Laskuvarjotaisto";
    resolution = sf::VideoMode(640, 480, 32);
    showFPS = false;
    showFPS = false;
    FPS = 60;
    windowSpawnX = 100;
    windowSpawnY = 100;
    vsync = true;
    fullscreen = false;
    allowDangerousReso = false;
    fullscreenMode = sf::VideoMode(1920, 1080, 32);
    wanted_fullscreen_w = 1920;
    wanted_fullscreen_h = 1080;
    controller_zero_tolerance = 30.f;
    globals.running = true;
    globals.mustExit = false;
}

int Window::updateWindow()
{
    pWindow->setFramerateLimit(FPS);

    if (fullscreen)
    {
        pWindow->setSize(sf::Vector2u(fullscreenMode.width, fullscreenMode.height));
    }
    else
    {
        pWindow->setSize(sf::Vector2u(resolution.width, resolution.height));
    }

    pWindow->setPosition(sf::Vector2i(windowSpawnX, windowSpawnY));
    pWindow->setTitle(titleText.c_str());
    pWindow->setVerticalSyncEnabled(vsync);

    return 0;
}

int Window::initWindow()
{
    // first create the window without setting its specific settings
    // This doubles as a test to make sure the window's properties can be tweaked

    if (fullscreen) {
        pWindow = RenderWindowPtr(new sf::RenderWindow(
            fullscreenMode, titleText, sf::Style::Fullscreen));
    }
    else {
        pWindow = RenderWindowPtr(new sf::RenderWindow(
            sf::VideoMode(800, 600), titleText.c_str(), sf::Style::Titlebar | sf::Style::Close));
    }

    sf::ContextSettings settings = pWindow->getSettings();

    fprintf(stderr, "Using OpenGL version %d.%d\n", settings.majorVersion, settings.minorVersion);

    if (settings.majorVersion < 3) {
        fprintf(stderr, "Your OpenGL version is less than 3. You may experience glitches\n");
    }

    if (updateWindow() != 0)
        return 1;

    return 0;
}

int Window::readConfig()
{
    std::string config_path = "/config.json";
    std::string config_path_try = globals.programPath + config_path;
    Json::Value root;
    Json::Reader reader;

    if (!utils.fileExists(config_path_try))
    {
        fprintf(stderr, "Did not find config.json in program dir");
        return 1;
    }

    std::ifstream configdata(config_path_try);
    bool parsingSuccessful = reader.parse(configdata, root);
    if (!parsingSuccessful)
    {
        fprintf(stderr, "Failed reading config.json contents: %s\n", reader.getFormatedErrorMessages().c_str());
        configdata.close();
        return 1;
    }

    titleText = root["title"].asString();
    std::string version = root["game_version"].asString();
    if (version.size() > NORMAL_STR_MAX) {
        fprintf(stderr, "version string too long, clamping\n");
        version = version.substr(0, NORMAL_STR_MAX);
    }
    strcpy(globals.gameVersion, version.c_str());

    resolution.width = root["resolution"].get("x", 800).asInt();
    resolution.height = root["resolution"].get("y", 640).asInt();

    showFPS = root["show_fps"].asString().compare("yes") == 0;
    FPS = root["framerate"].asInt();

    windowSpawnX = root["window_spawn"].get("x", 50).asInt();
    windowSpawnY = root["window_spawn"].get("y", 50).asInt();

    vsync = root["vsync"].asString().compare("yes") == 0;
    fullscreen = root["fullscreen"].asString().compare("yes") == 0;

    allowDangerousReso = root["allow_dangerous_resolutions"].asString().compare("yes") == 0;

    videoModes = sf::VideoMode::getFullscreenModes();

    // First set the fullscreen mode to current desktop size
    // Unfortunately does not get secondary monitor mode
    fullscreenMode = sf::VideoMode::getDesktopMode();

    // Then see if user has specified a custom resolution and -try- to use it
    // If this fails, go to windowed mode
    wanted_fullscreen_w = root["fullscreen_resolution"].get("x", -1).asInt();
    wanted_fullscreen_h = root["fullscreen_resolution"].get("y", -1).asInt();

    // Read joystick settings
    control_mapping[0] = parseJoystickControl(root["joystick_mappings"].get("joystick_0", "controller_punch").asString());
    control_mapping[1] = parseJoystickControl(root["joystick_mappings"].get("joystick_1", "controller_grab").asString());
    control_mapping[2] = parseJoystickControl(root["joystick_mappings"].get("joystick_2", "controller_punch").asString());
    control_mapping[3] = parseJoystickControl(root["joystick_mappings"].get("joystick_3", "controller_punch").asString());
    control_mapping[4] = parseJoystickControl(root["joystick_mappings"].get("joystick_4", "controller_punch").asString());
    control_mapping[5] = parseJoystickControl(root["joystick_mappings"].get("joystick_5", "controller_punch").asString());
    control_mapping[6] = parseJoystickControl(root["joystick_mappings"].get("joystick_6", "controller_select").asString());
    control_mapping[7] = parseJoystickControl(root["joystick_mappings"].get("joystick_7", "controller_start").asString());
    control_mapping[8] = parseJoystickControl(root["joystick_mappings"].get("joystick_8", "controller_punch").asString());
    control_mapping[9] = parseJoystickControl(root["joystick_mappings"].get("joystick_9", "controller_punch").asString());

    controller_zero_tolerance = root["joystick_zero_tolerance"].asFloat();

    return 0;
}

CONTROL_CODE Window::parseJoystickControl(const std::string& command)
{
    if (command.compare("controller_punch") == 0)
        return CONTROL_PUNCH;
    else if (command.compare("controller_grab") == 0)
        return CONTROL_GRAB;
    else if (command.compare("controller_select") == 0)
        return CONTROL_SELECT;
    else if (command.compare("controller_start") == 0)
        return CONTROL_START;
    else
        return CONTROL_UNUSED;
}

int Window::handleEvents()
{
    VERIFY_WINDOW(pWindow);

    sf::Event event;
    while (pWindow->pollEvent(event))
    {
        switch (event.type)
        {
            case sf::Event::Closed:
                mustExit = true;
                break;
            case sf::Event::Resized:
                break;
            case sf::Event::KeyReleased:
                switch (event.key.code)
                {
                    case sf::Keyboard::T:
                        break;
                }
                break;
            case sf::Event::KeyPressed:
                switch (event.key.code)
                {
                    case sf::Keyboard::Escape:
                        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_ESCAPE);
                        break;
                    case sf::Keyboard::RShift:
                    case sf::Keyboard::LShift:
                        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_SHIFT);
                        break;
                    case sf::Keyboard::Return:
                        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_ENTER);
                        break;
                    case sf::Keyboard::Up:
                        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_UP);
                        break;
                    case sf::Keyboard::Down:
                        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_DOWN);
                        break;
                }
                break;
            case sf::Event::JoystickButtonPressed:
            {
                active_joystick = event.joystickMove.joystickId;
                if (std::find_if(active_joysticks.begin(), active_joysticks.end(),
                             [&](const int& a) { return a == event.joystickMove.joystickId; }) == active_joysticks.end())
                    active_joysticks.push_back(event.joystickMove.joystickId);

                if (control_mapping.find(event.joystickButton.button) == control_mapping.end())
                {
                    fprintf(stderr, "Unmapped joystick key: %d\n", event.joystickButton.button);
                    continue;
                }

                pActiveScene->event(elapsed, event.joystickMove.joystickId, control_map_to_event(control_mapping[event.joystickButton.button]));

                break;
            }
            case sf::Event::JoystickButtonReleased:
                active_joystick = event.joystickMove.joystickId;
                break;
            case sf::Event::JoystickMoved:
                active_joystick = event.joystickMove.joystickId;
                if (std::find_if(active_joysticks.begin(), active_joysticks.end(),
                         [&](const int& a) { return a == event.joystickMove.joystickId; }) == active_joysticks.end())
                active_joysticks.push_back(event.joystickMove.joystickId);
                break;
            case sf::Event::JoystickConnected:
                active_joystick = event.joystickMove.joystickId;
                break;
            case sf::Event::JoystickDisconnected:
                active_joystick = event.joystickMove.joystickId;
                break;
        }
    }

    // Events for things like "left is constantly being pressed down"
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
    {
        // What happens when Left is constantly down
        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_LEFT_IS_PRESSED, -1.f);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
    {
        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_RIGHT_IS_PRESSED, 1.f);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_UP_IS_PRESSED, -1.f);
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        pActiveScene->event(elapsed, CONTROLLER_ID_KEYBOARD, EVENT_DOWN_IS_PRESSED, 1.f);
    }

    for (auto& joyid : active_joysticks)
    {
        // Events for joystick input
        float x_axis = sf::Joystick::getAxisPosition(joyid, sf::Joystick::X);
        float y_axis = sf::Joystick::getAxisPosition(joyid, sf::Joystick::Y);
        float z_axis = sf::Joystick::getAxisPosition(joyid, sf::Joystick::Z);
        float r_axis = sf::Joystick::getAxisPosition(joyid, sf::Joystick::R);

        CLAMP(x_axis, -INPUT_MAX_VAL, INPUT_MAX_VAL);
        CLAMP(y_axis, -INPUT_MAX_VAL, INPUT_MAX_VAL);
        CLAMP(z_axis, -INPUT_MAX_VAL, INPUT_MAX_VAL);
        CLAMP(r_axis, -INPUT_MAX_VAL, INPUT_MAX_VAL);

        if (x_axis > -controller_zero_tolerance && x_axis < controller_zero_tolerance)
            x_axis = 0;

        if (y_axis > -controller_zero_tolerance && y_axis < controller_zero_tolerance)
            y_axis = 0;

        if (z_axis > -controller_zero_tolerance && z_axis < controller_zero_tolerance)
            z_axis = 0;

        if (r_axis > -controller_zero_tolerance && r_axis < controller_zero_tolerance)
            r_axis = 0;

        pActiveScene->event(elapsed, joyid, EVENT_JOYSTICK_X_AXIS, x_axis / 100.f);
        pActiveScene->event(elapsed, joyid, EVENT_JOYSTICK_Y_AXIS, y_axis / 100.f);
        pActiveScene->event(elapsed, joyid, EVENT_JOYSTICK_Z_AXIS, z_axis / 100.f);
        pActiveScene->event(elapsed, joyid, EVENT_JOYSTICK_R_AXIS, r_axis / 100.f);
    }

    return 0;
}

EVENT Window::control_map_to_event(CONTROL_CODE c)
{
    switch (c)
    {
    case CONTROL_PUNCH:
        return EVENT_Z;
    case CONTROL_GRAB:
        return EVENT_X;
    case CONTROL_SELECT:
        return EVENT_SHIFT;
    case CONTROL_START:
        return EVENT_ENTER;
    }

    return EVENT_UNKNOWN;
}

int Window::init_scene()
{
    pActiveScene = ScenePtr(new Scene(pWindow));
    if (pActiveScene->init(pWindow) != 0)
        return 1;

    return 0;
}

int Window::main_loop()
{
    sf::Time delta;
    sf::Time last_frame;
    last_frame = game_clock.getElapsedTime();
    std::string title_str = titleText;
    int fps_frame_count = 0;
    std::list<int> fps_list;
    int fps_sum = 0;

    while (globals.running)
    {
        if (showFPS)
        {
            elapsed = game_clock.getElapsedTime();
            delta = elapsed - last_frame;
            last_frame = game_clock.getElapsedTime();
            fps_list.push_back((int)(1000 / (float)delta.asMilliseconds()));
            fps_sum += fps_list.back();
            if (fps_list.size() > 100)
            {
                fps_sum -= fps_list.front();
                fps_list.pop_front();
            }

            if (fps_frame_count > 100 && fps_list.size() > 0)
            {
                int fps = (int)(fps_sum / fps_list.size());
                fprintf(stderr, "FPS: %d\n", fps);
                std::stringstream ss;
                ss << title_str << ", " << fps << " FPS";
                pWindow->setTitle(ss.str());
                fps_frame_count = 0;
            }
            fps_frame_count++;
        }

        if (handleEvents() != 0)
            return 1;

        pWindow->clear();

        if (!pWindow->isOpen() || globals.mustExit)
        {
            globals.running = false;
            globals.mustExit = true;
        }

        if (pActiveScene != nullptr) {
            if (pActiveScene->update(elapsed, delta) != 0) {
                return 1;
            }

            if (pActiveScene->display(pWindow) != 0) {
                return 1;
            }

            if (pActiveScene->must_exit) {
                globals.mustExit = true;
            }
        }

        if (pWindow->isOpen())
        {
            pWindow->display();
        }
    }

    return 0;
}

} // namespace
