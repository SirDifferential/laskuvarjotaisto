#ifndef SCENE_H
#define SCENE_H

#include "types.h"

namespace lvt {

typedef enum {
    WaitPlayers = 0,
    InGame = 1,
    ChuteOpen = 2
} SceneState;

class Scene
{
public:
    Scene(RenderWindowPtr rwindow);

    int event(const sf::Time& event_time, const int& controller_id, const EVENT& code);
    int event(const sf::Time& event_time, const int& controller_id, const EVENT& code, const float& data);

    /**
     * @brief Main logic function
     * @param update_time: Time since program was started
     * @param delta: Time since last call to this function
     */
    int update(const sf::Time& update_time, const sf::Time& delta);
    int display(RenderWindowPtr window);
    int init(RenderWindowPtr rwindow);
    int addDude(int id);
    int drawLoading(RenderWindowPtr rwindow, const std::string& text);

    SceneState scenestate;

    bool must_exit;
    sf::View view;

    /**
     * @brief Where the camera is currently centered
     */
    sf::Vector2f lookat;
    float zoom_level;
    std::vector<DudePtr> dudes;

    ChuteSpritePtr menu_bg;
    ChuteSpritePtr controls_img;
    ChuteSpritePtr bg;
    ChuteSpritePtr marker;

    ChuteSpritePtr meter;
    ChuteSpritePtr meter_cursor;

    /** Which controller id controls which dude */
    std::unordered_map<int, DudePtr> control_target;

    MusicPtr music_charselect;
    MusicPtr music_ingame;

    // How long until ground is hit
    int scene_length_ms;
    sf::Time scene_start_time;

    float level_percent;

    int bomb_sfx_duration_ms;
    int wait_until_lobby_ms;

    sf::Time chute_open_time;
    bool blort_played;

    TextPtr menu_text;
    FontPtr font;
    bool is_loading;
};

} // namespace

#endif // SCENE_H
